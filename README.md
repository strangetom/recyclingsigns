# RecyclingSigns

SVG recyling signs based on UK local authority signs

[Automotive](#automotive)  
[Electrical](#electrical)  
[Glass](#glass)  
[Hazardous household waste](#hazardous-household-waste)  
[Metal](#metal)  
[Organic](#organic)  
[Other](#other)  
[Paper and card](#paper-and-card)  
[Plastics](#plastics)  
[Textiles](#textiles)  

## Automotive
![](png/automotive_waste.png)
![](png/car_batteries.png)
![](png/tyres.png)
![](png/used_engine_oil.png)

## Electrical
![](png/batteries.png)
![](png/fluorescent_tubes.png)
![](png/fridge_freezer.png)
![](png/large_electrical_appliances.png)
![](png/mobile_phones.png)
![](png/small_electrical_appliances.png)
![](png/tvs_monitors.png)

## Glass
![](png/blue_glass.png)
![](png/brown_glass.png)
![](png/clear_glass.png)
![](png/flat_glass.png)
![](png/glass.png)
![](png/green_glass.png)
![](png/mixed_glass.png)

## Hazardous household waste
![](png/asbestos.png)
![](png/gas_bottles.png)
![](png/household_&_garden_chemicals.png)
![](png/pesticides.png)

## Metal
![](png/aerosols.png)
![](png/aluminium_cans.png)
![](png/food_tins_drink_cans.png)
![](png/scrap_metal.png)
![](png/steel_cans.png)

## Organic
![](png/garden_waste.png)
![](png/kitchen_waste.png)

## Other
![](png/dry_mixed_recyclables.png)
![](png/furniture.png)
![](png/household_waste.png)
![](png/paint.png)
![](png/tapes_discs.png)

## Paper and card
![](png/cardboard.png)
![](png/mixed_paper_card.png)
![](png/newspapers_magazines.png)
![](png/paper.png)

## Plastics
![](png/plastic_bags.png)
![](png/plastic_bottles.png)
![](png/plastics.png)

## Textiles
![](png/clothes.png)
![](png/mixed_textiles_clothes.png)
![](png/shoes.png)
![](png/textiles.png)

